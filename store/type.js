
export const ADD_USER = "ADD_USER";

export const SHOW_WINE = "SHOW_WINE";

export const SHOW_USER = "SHOW_USER";

export const SEARCH_WINE = "SEARCH_WINE";

export const WHERE_AM_I = 'WHERE_AM_I';

export const SHOW_COMENTS_FROM_POST = "SHOW_COMENTS_FROM_POST";

export const SHOW_COMENTS = 'SHOW_COMENTS';