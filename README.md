	“Accept what life offers you and try to drink from every cup. All wines should be tasted; some should only be sipped, but with others, drink the whole bottle.” - Paulo Coelho, Brida

O vinho é uma bebida que contém imenso valor cultural para Portugal, sendo cultivado desde o século X A.C até aos dias de hoje. (cf. secção 2. Enquadramento Teórico). Hoje em dia há a possibilidade de ir a uma superfície comercial e escolher um vinho dentro de um diverso leque de escolhas disponíveis. No entanto, a qualidade da escolha é sempre dotada de subjetividade.

Nos dias de hoje as pessoas remetem cada vez menos para a opinião de um especialista, sendo que os ratings ou reviews feitos por outros a um determinado produto se tornaram o padrão da escolha. Com a existência das apps, cada utilizador acredita que pode contribuir a qualquer altura e que a sua contribuição será valorizada. Por isso mesmo, a opinião de um enólogo pode se tornar menos relevante que a opinião do público, democratizando assim a valorização de um produto.

Aplicações tais como Vivino ou Vinipad possuem uma infraestrutura e capacidades técnicas (tais como scan de imagem em realidade aumentada) que ajudam o utilizador a obter a informação que procura. No entanto, pudemos constatar que não existe muita variedade de apps dentro deste campo.

Face a esta concorrência, é necessária uma plataforma em que os utilizadores possam, além de obter informação acerca dos vinhos, inserir-se dentro de uma comunidade crescente. A ideia da aplicação LAGAR é ir para além do que já existe, juntando o conceito de cultura participativa a toda a informação e contexto que há à volta do vinho, através da criação de uma plataforma para a comunidade, onde possam partilhar as suas experiências e crescer como apreciadores de vinho.

Por isto tudo, vamos implementar uma aplicação mobile, também podendo ser chamada por adega virtual, que consiga envolver todos os aspetos positivos já existentes nas aplicações e tentar otimizá-las para que a experiência do utilizador seja positiva.

A nossa aplicação vai ter como público-alvo homens e mulheres a partir dos 18 anos que possam incluir-se em um (ou mais do que um) destes parâmetros: apreciadores de vinho, curiosos sobre esta temática, indecisos sobre que vinho beber/comprar, interessados em ter a sua coleção de vinhos em formato digital, enólogos e pessoas com interesse na vinícola portuguesa.

As tarefas apresentadas neste relatório foram realizadas de forma a que facilitem e “encorajem” a realização da tarefa seguinte. Exemplo disso são os sketches serem seguidos pelos wireframes, ou os cenários de utilização serem utilizados para a criação dos requisitos funcionais e não funcionais.

Esta lógica de realização de tarefas tem como finalidade tornar a programação da aplicação em algo mais simples e intuitivo, reduzindo erros e removendo incertezas do processo, levando a uma mais rápida implementação e posterior teste desta.
